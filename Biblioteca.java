
package biblioteca;
import java.util.Scanner;


public class Biblioteca {

   
public static int busquedaBinariaConWhile(String[] arreglo, String busqueda) {
    
    int izquierda = 0, derecha = arreglo.length - 1;
 
    while (izquierda <= derecha) {
        
        int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
        String elementoDelMedio = arreglo[indiceDelElementoDelMedio];
       
        int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);
 
 
        if (resultadoDeLaComparacion == 0) {
            return indiceDelElementoDelMedio;
        }
 
        if (resultadoDeLaComparacion < 0) {
            derecha = indiceDelElementoDelMedio - 1;
        } else {
            izquierda = indiceDelElementoDelMedio + 1;
        }
    }
    
    return -1;
}
    public static void main(String[] args) {
        String [][] buscar = { {"Asignatura","Autor", "Título"}, {"Computacion", "Barry Burd", "Java para tontos" }, {"Lenguas", "Patricia Martinez", "Diccionario Maya" },{"Historia","Mario H. Aranda González","HistóricoGeografía del Estado de Campeche" }};


            Scanner teclado = new Scanner(System.in);
            String opc = " ";
            do {
            System.out.println();
            System.out.print("----- Menú Principal -----"
            + "\nA. Ver la Lista"
            + "\nB. Buscar Asignatura"
            + "\nC. Salir"
            + "\n");

            System.out.println("Seleccione su desición");
            opc = teclado.nextLine();

            switch (opc) {
            case "a":
            case "A":
                String cad="";
                for(int i=0;i<4;i++){
                for(int j=0;j<3;j++){
                cad+=buscar[i][j]+" ";
                }
                cad+="\n";
                }
                System.out.println(cad);
            break;

            case "b":
                    case "B": 

                        String[] arreglo = { "Asignatura","Computación", "Lenguas", "Historia" };

                    String busqueda = "Computación";
                        int indiceDelElementoBuscado = busquedaBinariaConWhile(arreglo, busqueda);

                        for(int i=0;i<4;i++){
                                if(buscar[i][0].equals(busqueda)){
                                     System.out.println( busqueda + " Se localiza en el index  " + indiceDelElementoBuscado+"\n"+ buscar[i][0]+" "+buscar[i][1]+" "+ buscar[i][2]);
                                }
                        }    

                            break;


                    case "c":
                    case "C":
                        System.out.println();
                        System.out.println("Gracias, vuelva pronto");
            }

            }while(!opc.equalsIgnoreCase("C"));

}
    }
